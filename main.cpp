#include <iostream>
#include <string>
#include <cmath>
using namespace std;

void mengulang()
{
	cout << "Apakah anda ingin kembali ke menu utama? (Y/N) ";
    char jawab;
    cin >> jawab;
    if (jawab == 'N' || jawab == 'n')
    {
        cout << "Terimakasih telah menggunakan layanan kami.";
		exit(0);
    }
	else if ((jawab != 'Y' && jawab != 'y') && (jawab != 'N' && jawab != 'n'))
	{
		cout << "Inputan anda tidak valid.";
		cout << "Terimakasih telah menggunakan layanan kami.";
		exit(0);
	}
}
int main ()
{
	int a;
	cout << "Selamat datang di ATM Komunis (Bersama)" << endl;
	awal:
	string nama = "Dian Saputra";
    int saldo[1] = {1000000};
    cout << "Masukkan nomor rekening anda: ";
    string nomor_rekening;
    cin >> nomor_rekening;
    cout << "Masukkan password: ";
    string password;
    cin >> password;
    if (nomor_rekening == "0111" && password == "2002")
	{
		cout << "Selamat datang " << nama << endl;
		goto ulang;
	}
	else if (nomor_rekening != "0111" && password != "2002")
	{
		cout << "Nomor rekening dan password salah" << endl;
		for (int i = 0; i < 2; i++)
		{
			cout << "Kesempatan tersisa : " << 2 - i << endl;
			cout << "Masukkan nomor rekening anda: ";
			cin >> nomor_rekening;
			cout << "Masukkan password: ";
			cin >> password;
			if (nomor_rekening == "0111" && password == "2002")
			{
				cout << "Selamat datang " << nama << endl;
				goto ulang;
			}
		}
		cout << "Maaf, anda telah melakukan percobaan login sebanyak 3 kali." << endl;
		cout << "Terimakasih telah menggunakan layanan kami.";
		exit(0);
	}
    ulang:
    cout << "Ada yang bisa kami bantu? (Y/N) ";
    char jawab;
    cin >> jawab;
    if (jawab == 'Y' || jawab == 'y')
    {
        goto list;
    }
    else if (jawab == 'N' || jawab == 'n')
    {
        cout <<"Terimakasih telah menggunakan layanan kami.";
        return 0;
    }
    else if (jawab != 'Y' || jawab != 'y' || jawab != 'N' || jawab != 'n')
    {
        cout << "Maaf, input yang anda masukkan salah." <<endl;
        cout << "Silahkan ulangi kembali." << endl;
        goto ulang;
    }

    list :
    {
    	cout << "1. Cek saldo" << endl;
    	cout << "2. Transfer" << endl;
    	cout << "3. Tarik tunai" << endl;
		cout << "4. Setor tunai" << endl;
		cout << "5. Keluar" << endl;
		cout << "Pilih menu yang tersedia: ";
    }
    int menu;
    cin >> menu;
    if (menu == 1)
    {
        cout << "Saldo anda sebesar Rp. "<<saldo[0]<< endl;
        mengulang();
		if (jawab == 'Y' || jawab == 'y')
    	{
        	goto list;
    	}  
    }	
    else if (menu == 2)
    {
    	cout << "Masukkan nomor rekening tujuan: ";
        string nomor_rekening_tujuan;
        cin >> nomor_rekening_tujuan;
        cout << "Masukkan jumlah transfer: ";
        int jumlah_transfer;
        cin >> jumlah_transfer;
        if (jumlah_transfer <= saldo[0])
        {
            cout << "Transfer berhasil" << endl;
			saldo [0] = saldo [0] - jumlah_transfer;
            cout << "Sisa saldo anda sebesar Rp. "<<saldo[0]<< endl;
			mengulang();
			if (jawab == 'Y' || jawab == 'y')
			{
				goto list;
			}  
        }
        else
        {
            cout << "Transfer gagal karena saldo tidak mencukupi" << endl;
            mengulang();
			if (jawab == 'Y' || jawab == 'y')
			{
				goto list;
			}
        }     
    }
    else if (menu == 3)
    {
        cout << "Masukkan jumlah tarik tunai: ";
        int jumlah_tarik_tunai;
        cin >> jumlah_tarik_tunai;
        if (jumlah_tarik_tunai <= saldo[0])
        {
            saldo[0] = saldo[0] - jumlah_tarik_tunai;
            cout << "Tarik tunai berhasil" << endl;
            cout << "Sisa saldo anda sebesar Rp. "<<saldo[0]<< endl;
			mengulang();
			if (jawab == 'Y' || jawab == 'y')
			{
				goto list;
			}
        }
        else if (jumlah_tarik_tunai > saldo[0])
        {
            cout << "Saldo anda tidak mencukupi" << endl;
            mengulang();
			if (jawab == 'Y' || jawab == 'y')
			{
				goto list;
			}
        }
	}
    else if (menu == 4)
    {
        cout << "Masukkan jumlah setor tunai: ";
        int jumlah_setor_tunai;
        cin >> jumlah_setor_tunai;
        saldo[0] = saldo[0] + jumlah_setor_tunai;
        cout << "Setor tunai berhasil" << endl;
        cout << "Sisa saldo anda sebesar Rp. "<<saldo[0]<< endl;
		mengulang();
		if (jawab == 'Y' || jawab == 'y')
		{
			goto list;
		}
    }
    else if (menu == 5)
    {
        cout << "Terima kasih telah menggunakan ATM" << endl;
    }
    else
    {
        cout << "Menu tidak tersedia" << endl;
        cout << "Terima kasih telah menggunakan ATM" << endl;	
    }
	return 0;
}

